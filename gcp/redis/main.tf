terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.19"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.19"
    }
  }
}

resource "google_redis_instance" "cache" {
    name = replace(
      replace(substr("${var.labels.unfurlproject}${var.labels.deployment}${var.name}", -40, 40), "_", "-"),
      "/^([0-9]|-)/",
      "r"
    )
    tier = var.tier
    display_name = var.name
    memory_size_gb = var.memory_size_gb
    labels = var.labels
}
