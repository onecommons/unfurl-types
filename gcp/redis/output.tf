output "redis_host" {
  description = "Redis instance hostname"
  value       = google_redis_instance.cache.host
}

output "instance_id" {
  description = "Redis instance ID"
  value       = google_redis_instance.cache.id
}

output "port" {
  description = "RDS instance port"
  value       = google_redis_instance.cache.port
}

