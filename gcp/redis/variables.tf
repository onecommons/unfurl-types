variable memory_size_gb {
  description = "Redis memory size in GiB."
  type = number
  default = 1
}

variable "tier" {
  description = "BASIC or STANDARD_HA"
  type = string
  default = "BASIC"
}

variable "labels" {
  default = {}
  type    = map(string)
}

variable "name" {
  type    = string
  default = ""
}
