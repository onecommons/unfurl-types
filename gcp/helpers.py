import os
import re
from unfurl.configurator import Configurator


def get_compute_resource_name(resource_id):
    """Get GCP compute console URL for a given resource ID."""
    resource_id_regex = (
        r"^projects/([A-z]|[0-9]|-)*/zones/([A-z]|[0-9]|-)*/instances/([A-z]|[0-9]|-)*$"
    )
    resource_id_pattern = re.compile(resource_id_regex)

    if not resource_id_pattern.match(resource_id):
        raise ValueError(
            f"Invalid resource ID, resource ID must match {resource_id_regex}"
        )

    # id format: projects/{{project}}/zones/{{zone}}/instances/{{name}}
    split = resource_id.split("/")

    return {"project": split[1], "zone": split[3], "name": split[5]}


def get_compute_console_url(ctx, args):
    resource_id = args["resource_id"]
    parsed = get_compute_resource_name(resource_id)

    gcp_console_format = "https://console.cloud.google.com/compute/instancesDetail/zones/{zone}/instances/{name}?project={project}"
    return gcp_console_format.format(**parsed)


def get_gcpsql_console_url(ctx, args):
    """Get GCP SQL console URL for a given connection name."""
    connection_name_regex = r"^([A-z]|[0-9]|-)*:([A-z]|[0-9]|-)*:([A-z]|[0-9]|-)*$"
    connection_name_pattern = re.compile(connection_name_regex)
    gcp_console_format = "https://console.cloud.google.com/sql/instances/{name}/overview?project={project}"

    connection_name = args["name"]

    if not connection_name_pattern.match(connection_name):
        raise ValueError(
            f"Invalid resource ID, resource ID must match {connection_name_pattern}"
        )

    # id format: {{project}}:{{region}}:{{instance}}
    split = connection_name.split(":")

    parsed = {"project": split[0], "zone": split[1], "name": split[2]}
    return gcp_console_format.format(**parsed)


def get_memorystorage_console_url(ctx, args):
    instance_id_regex = r"^projects/([A-z]|[0-9]|-)*/locations/([A-z]|[0-9]|-)*/instances/([A-z]|[0-9]|-)*$"
    instance_id_pattern = re.compile(instance_id_regex)
    gcp_console_format = "https://console.cloud.google.com/memorystore/redis/locations/{location}/instances/{name}/details/overview?project={project}"

    instance_id = args["instance_id"]
    if not instance_id_pattern.match(instance_id):
        raise ValueError(
            f"Invalid resource ID, resource ID must match {instance_id_regex}"
        )
    # format: projects/{{project}}/locations/{{location}}/instances/{{name}}

    split = instance_id.split("/")
    parsed = {"project": split[1], "location": split[3], "name": split[5]}
    return gcp_console_format.format(**parsed)


def get_clouddns_console_url(ctx, args):
    pass


def get_project_and_zone():
    project = os.getenv("CLOUDSDK_CORE_PROJECT")
    if not project:
        raise ValueError(
            "Can't choose machine type - CLOUDSDK_CORE_PROJECT not defined"
        )
    zone = os.getenv("CLOUDSDK_COMPUTE_ZONE")
    if not zone:
        raise ValueError(
            "Can't choose machine type - CLOUDSDK_COMPUTE_ZONE not defined"
        )
    return project, zone


class GCPComputeInstanceConfigurator(Configurator):
    def run(self, task):
        instance_id = task.target.attributes["id"]
        success = bool(instance_id)
        if instance_id and task.configSpec.operation == "restart":
            success = self.restart_compute_instance(task, instance_id)
        yield task.done(success)

    # https://cloud.google.com/compute/docs/instances/stop-start-instance
    def restart_compute_instance(self, task, instance_id):
        from google.cloud.compute_v1 import InstancesClient

        client = InstancesClient()
        parsed = get_compute_resource_name(instance_id)

        instance = parsed["name"]
        task.logger.verbose(f"Stopping instance {instance}")
        request = client.stop(
            project=parsed["project"], zone=parsed["zone"], instance=instance
        )
        timeout = task.configSpec.timeout or 180
        try:
            request.result(timeout=timeout)
        except Exception:
            task.logger.error(
                f"Exception when stopping instance {instance}", exc_info=1
            )
            # don't return, try starting anyway
        else:
            if request.error_code:
                task.logger.error(
                    f"Error stopping instance {instance}: [Code: {request.error_code}]: {request.error_message}"
                )
                return False

        task.logger.verbose(f"Starting instance {instance}")
        request = client.start(
            project=parsed["project"], zone=parsed["zone"], instance=instance
        )
        if request.error_code:
            task.logger.error(
                f"Error starting instance {instance}: [Code: {request.error_code}]: {request.error_message}"
            )
        return not request.error_code


class MetadataConfigurator(Configurator):
    def run(self, task):
        task.logger.info("Fetching machine types")
        task.target.attributes["machine_types"] = list(self.all_machine_types(task))
        task.logger.info("Picking zone")
        task.target.attributes["zone"] = self.pick_zone(task)
        yield task.done(True)

    def can_dry_run(self, task):
        return True

    @staticmethod
    def all_machine_types(task):
        # delay imports until now so the python package can be installed first
        from google.cloud.compute_v1 import ListMachineTypesRequest, MachineTypesClient

        request = ListMachineTypesRequest()
        project, zone = get_project_and_zone()
        request.project = project
        request.zone = zone

        try:
            client = MachineTypesClient()
            response = client.list(request)
        except Exception as e:
            task.logger.error("GCP: %s", e)
            raise ValueError("Can't find machine types. Can't communicate with GCP.")

        yield from (
            {
                "name": item.name,
                "mem": item.memory_mb,
                "cpu": item.guest_cpus,
            }
            for item in response.items
        )

    @staticmethod
    def pick_zone(task):
        _p, z = get_project_and_zone()
        print(f"Got zone: {z}")
        task.logger.info(f"Got zone: {z}")
        return z


if __name__ == "__main__":
    print(
        get_compute_console_url(
            None, {"resource_id": "projects/test/zones/test/instances/test"}
        )
    )
