output "host" {
  description = "The ip addresss assigned to the instance"
  value       = google_sql_database_instance.default.public_ip_address
}

output "connection_name" {
  sensitive = true
  value     = google_sql_database_instance.default.connection_name
}
