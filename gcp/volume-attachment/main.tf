terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.19"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.19"
    }
  }
}


variable "disk_label" {
  description = "Label of the disk when attached"
  type        = string
}

variable "disk_id" {
  description = "Id of the disk to attach"
  type        = string
}

variable "instance_id" {
  description = "Id of instance to which the volume needs to be attached."
  type        = string
}

variable "persistent" {
  description = "Keep the volume after associated instance is deleted"
  type        = bool
  default     = true
}
variable "tags" {
  type    = map(string)
  default = {}
}




resource "google_compute_attached_disk" "attachment" {
  device_name = var.disk_label
  disk        = var.disk_id
  instance    = var.instance_id
}
