# using $CI_BUILDS_DIR because I keep getting dubious ownership errors (not sure why that helps)
variables:
  OC_URL: https://unfurl.cloud

  OC_NAMESPACE: onecommons/blueprints
  DEPLOY_IMAGE: ghcr.io/onecommons/unfurl:main
  UNFURL_SERVER_IMAGE: $DEPLOY_IMAGE-server

  # WARNING: this will probably be overwritten by the instance settings if not set in project CI
  # WARNING: this might be circular depending on where this repository is hosted
  UNFURL_PACKAGE_RULES: >
    gitlab.com/onecommons/unfurl-types $CI_SERVER_URL/$CI_PROJECT_PATH#$CI_COMMIT_BRANCH
    unfurl.cloud/onecommons/unfurl-types $CI_SERVER_URL/$CI_PROJECT_PATH#$CI_COMMIT_BRANCH

  UNFURL_CLOUD_SERVER: $OC_URL
  UNFURL_LOGGING: debug
  # NOT UNFURL_LOGFILE
  UNFURL_SERVICE_LOGFILE: $CI_BUILDS_DIR/unfurl.log # Can't be project dir because the service starts too early
  UNFURL_SERVICE_JOB_LOG_MAX_LEN: 350000
  UNFURL_CLONE_ROOT: $CI_BUILDS_DIR/repos
  # CACHE_TYPE: RedisCache
  # CACHE_REDIS_URL: redis://redis
  UNFURL_HOME:
  PY_COLORS: "1"
  GUNICORN_CMD_ARGS: "-w1 -t999 --log-level debug -b 0.0.0.0:5000 --capture-output --log-file $UNFURL_SERVICE_LOGFILE"
  UNFURL_TEST_SERVER_DEBUG_PATCH: "1"

  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"
  UNFURL_SERVER_URL: http://ufserver:5000
  SPEC_GLOBS: >
    aws__nextcloud__aws-postgres.*
    aws__nextcloud__memorydb.*
    aws__nextcloud__volume.*
    aws__wordpress__wordpress-aws-mysql.*
    aws__wordpress__wordpress.*
    aws__mediawiki__aws-mariadb.*
    az__nextcloud__pg.*
    gcp__nextcloud__gcp-postgres.*
    gcp__nextcloud__memorystore.*
    gcp__nextcloud__volume.*
    cachet.*
    node-filter*
    *__nestedcloud.*
  # if testing nestedcloud - ensure it is available in the namespace you are testing
  # the most convenient way of doing this is probably to transfer the project back and forth between namespaces to create a redirect

  CI_DEBUG_SERVICES: "true"

test:
  stage: test

  image: 
    name: registry.unfurl.cloud/onecommons/unfurl-gui

  cache:
    paths:
      - $CI_PROJECT_DIR/cache/Cypress
      - $CI_PROJECT_DIR/node_modules

  services:
    # - name: redis
    - name: $UNFURL_SERVER_IMAGE
      # can't mount unfurl-types https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28121
      alias: ufserver

  script:
    - git config --global --add safe.directory $CI_PROJECT_DIR # for when the cache wants to bug out?
    - curl -s http://ufserver:5000/health
    - echo 'ENSURE YOU HAVE THE CORRECT PACKAGE RULES:' $UNFURL_PACKAGE_RULES
    - mkdir $CI_PROJECT_DIR/node_modules || /bin/true
    - git clone https://github.com/onecommons/unfurl-gui --branch cy-tests --depth 1
    - cd unfurl-gui
    - pushd ./cypress/e2e/blueprints; SPECS=$((ls -w0 -x $SPEC_GLOBS 2>/dev/null || /bin/true) | sed -E 's!(^|\s+)!,./cypress/e2e/blueprints/!g' | sed 's!^,!!'); popd
    - pushd ./cypress/e2e/deployments; SPECS=$SPECS$((ls -w0 -x $SPEC_GLOBS 2>/dev/null || /bin/true) | sed -E 's!(^|\s+)!,./cypress/e2e/deployments/!g'); popd
    - echo $SPECS
    - ln -s $CI_PROJECT_DIR/node_modules
    - yarn install
    - yarn run integration-test run  --namespace $OC_NAMESPACE -- --browser chrome -s "$SPECS" -e DRYRUN=true

  after_script:
    - echo -e "\e[0Ksection_start:`date +%s`:unfurl_log[collapsed=true]\r\e[0KUnfurl Server logs"; tail --bytes $UNFURL_SERVICE_JOB_LOG_MAX_LEN $CI_BUILDS_DIR/unfurl.log; echo -e "\e[0Ksection_end:`date +%s`:unfurl_log\r\e[0K"
    - mv $CI_BUILDS_DIR/unfurl.log $CI_PROJECT_DIR/unfurl.log


  artifacts:
    when: always
    paths:
      - $CI_PROJECT_DIR/unfurl-gui/cypress/screenshots/**/*.png
      - $CI_PROJECT_DIR/unfurl-gui/cypress/screenshots/**/*.json
      - $CI_PROJECT_DIR/unfurl.log
    expire_in: never


  rules:
    - if: $CY_TEST # most instances won't want to run tests when unfurl-types is synced
    - if: $CI_PIPELINE_SOURCE != "push"
      when: never
