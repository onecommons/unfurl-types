/* ========================================================================== */
/* CONFIGURATION OPTIONS / ENVIRONMENT VARIABLES                              */
/* ========================================================================== */

terraform {
  required_providers {
    cloudinit = {
      source  = "hashicorp/cloudinit"
      version = "2.2.0"
    }
  }
}


variable "cloud_init" {
  description = "Cloud-init YAML to convert to instance user data"
  type        = string
  default     = ""
  sensitive   = true # contains passwords in env
}
variable "cloudinit_part" {
  description = "Additional cloud-init configuration used to setup and/or customise the instance beyond the defaults provided by this module."
  type        = list(object({ content_type: string, content: string }))
  default     = []
}

// Generate cloud-init config

data "cloudinit_config" "config" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "cloud-init.yaml"
    merge_type   = "list(append)+dict(no_replace,recurse_list)+str()"
    content_type = "text/cloud-config"
    content      = var.cloud_init
  }

  # Add any additional cloud-init configuration or scripts provided by the user
  dynamic "part" {
    for_each = var.cloudinit_part
    content {
      merge_type   = "list(append)+dict(no_replace,recurse_list)+str()"
      content_type = part.value.content_type
      content      = part.value.content
    }
  }
}


output "cloud_config" {
  description = "Content of the cloud-init config to be deployed to a server."
  value       = data.cloudinit_config.config.rendered
}
