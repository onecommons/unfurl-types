variable machine_type  {
    description = "The instance class used."
    default = "cache.t4g.micro"
}

variable "name" {
  default = "redis"
  type    = string
}

variable "availability_zone" {
  description = "A reference to the zone where the disk resides."
  type        = string
}

variable "port" {
  default = "6379"
}

variable "tags" {
  type    = map(string)
  default = {}
}
