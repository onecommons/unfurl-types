output "redis_host" {
  description = "Redis instance hostname"
  value       = aws_elasticache_cluster.redis.cache_nodes.0.address
}

output "port" {
  description = "RDS instance port"
  value       = aws_elasticache_cluster.redis.port
}

output "ec_arn" {
  description = "RDS Amazon Resource Name"
  value = aws_elasticache_cluster.redis.arn
}
