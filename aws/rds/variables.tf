variable "instance_class" {
  description = "The instance type of the RDS instance."
  default     = "db.t2.micro"
}
variable "engine" {
  description = "The database engine to use."
}
variable "engine_version" {
  description = "Engine Version"
  default     = "5.1"
}
variable "dbuser" {
  description = "Username for the master DB user."
}
variable "dbpass" {
  description = "Password for the master DB user."
  sensitive   = true
}
variable "dbname" {
  description = "The name of the database to create when the DB instance is created. Default: null (don't create a database."
  default     = null
}

variable "port" {
  type        = number
  description = "port to open for security group"
}
variable "deletion_protection" {
  type    = bool
  default = true
}

variable "tags" {
  type    = map(string)
  default = {}
}
