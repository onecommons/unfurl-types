provider "aws" {
  default_tags {
    tags = var.tags
  }
}


variable "name" {
  description = "Name of the volume resource"
  type        = string
}
variable "ebs_size" {
  description = "Size of the persistent disk, specified in GB."
  type        = number
}
variable "availability_zone" {
  description = "A reference to the zone where the disk resides."
  type        = string
}
variable "tags" {
  type    = map(string)
  default = {}
}


resource "aws_ebs_volume" "volume" {
  availability_zone = var.availability_zone
  size              = var.ebs_size
  tags = {
    Name = "${var.name}"
  }
}


output "volume_az" {
  value = aws_ebs_volume.volume.availability_zone
}
output "volume_id" {
  value = aws_ebs_volume.volume.id
}
